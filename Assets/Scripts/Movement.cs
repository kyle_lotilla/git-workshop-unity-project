using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
// Hello World
    [SerializeField]
    private float velocity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;

        if (Input.GetKey(KeyCode.A))
        {
            pos.x -= velocity;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            pos.x += velocity;
        }

        if (Input.GetKey(KeyCode.W))
        {
            pos.y += velocity;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            pos.y -= velocity;
        }

        transform.position = pos;
    }
}
